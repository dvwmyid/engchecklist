<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Genset 1 - Engine Check') }}
        </h2>
    </x-slot>
        
    <form action="{{ route('gensets.store') }}" method="post">
        @csrf
        
        <div class="space-y-2">
            <div class="">
                <label class="block text-sm font-medium text-gray-700" for="speed">Speed</label>
                <input placeholder="example:1500" type="number" step="0.01" name="speed" class="block w-full mt-1 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded">
            </div>
            <div class="">
                <label class="block text-sm font-medium text-gray-700" for="oil_pressure">Oil Pressure</label>
                <input type="number" step="0.01" name="oil_pressure" class="block w-full mt-1 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded">
            </div>
            <div class="">
                <label class="block text-sm font-medium text-gray-700" for="coolant_temp">Coolant Temperature</label>
                <input type="number" step="0.01" name="coolant_temp" class="block w-full mt-1 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded">
            </div>
            <div class="">
                <label class="block text-sm font-medium text-gray-700" for="battery_volt">Battery Voltage (Bar)</label>
                <input type="number" step="0.01" name="battery_volt" class="block w-full mt-1 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded">
            </div>
            <div class="">
                <label class="block text-sm font-medium text-gray-700" for="run_time">Run Time</label>
                <input type="number" step="0.01" name="run_time" class="block w-full mt-1 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded">
            </div>

        </div>
        <div class="mt-4">
            <button type="submit" id="submit" onclick="return derry();" class="block w-full text-center py-2 rounded-md bg-blue-500 text-white hover:bg-blue-600">Submit</button>
        </div>
    </form>
    
    <script>
        function derry(){
            if(confirm('dah yakin sama input nya bro?')){
                document.getElementById('submit').submit();
            } else{
                return false;
            }
        }
    </script>
    
    
</x-app-layout>