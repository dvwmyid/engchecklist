<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Update Transport') }}
        </h2>
    </x-slot>
    
    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong>Ada Eror, cek ulang! <br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    <form action="{{ route('transports.update', $transport->id) }}" method="post">
        @csrf
        @method('PUT')
        
        <div class="space-y-2">
            <div class="">
                <label class="block text-sm font-medium text-gray-700" for="date_run">Date</label>
                <input type="date" value="{{ $transport->date_run->format('Y-m-d') }}" name="date_run" class="block w-full mt-1 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded" Placeholder="date">
            </div>
            <div class="">
                <label class="block text-sm font-medium text-gray-700" for="vehicle">Vehicle</label>
                <select name="vehicle" id="vehicle" class="block w-full mt-1 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded">
                    @foreach ($vehicles as $vehicle)
                        <option value="{{ $vehicle->id }}" {{ $transport->vehicle_id === $vehicle->id ? 'selected' : '' }}>{{ $vehicle->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="">
                <label class="block text-sm font-medium text-gray-700" for="purpose">Purpose</label>
                <select name="purpose" id="purpose" class="block w-full mt-1 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded">
                    @foreach ($purposes as $purpose)
                        <option value="{{ $purpose->id }}" {{ $transport->purpose_id === $purpose->id ? 'selected' : '' }}>{{ $purpose->description }}</option>
                    @endforeach
                </select>
            </div>
            <div class="">
                <label class="block text-sm font-medium text-gray-700" for="from_time">From (Time)</label>
                <input type="time" value="{{ $transport->from_time->format('H:m') }}" step="60" name="from_time" class="block w-full mt-1 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded">
            </div>
            <div class="">
                <label class="block text-sm font-medium text-gray-700" for="to_time">To (Time)</label>
                <input type="time" value="{{ $transport->to_time->format('H:m') }}" step="60" name="to_time" class="block w-full mt-1 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded">
            </div>
            <div class="">
                <label class="block text-sm font-medium text-gray-700" for="km_before">(KM) Before</label>
                <input type="number" value="{{ $transport->km_before }}" name="km_before" class="block w-full mt-1 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded">
            </div>
            <div class="">
                <label class="block text-sm font-medium text-gray-700" for="km_after">(KM) After</label>
                <input type="number" value="{{ $transport->km_after }}" name="km_after" class="block w-full mt-1 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded">
            </div>
            <div class="">
                <label class="block text-sm font-medium text-gray-700" for="remarks">Remarks</label>
                <input type="text" value="{{ $transport->remarks }}" name="remarks" class="block w-full mt-1 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded">
            </div>
        </div>
        <div class="mt-4">
            <button type="submit" class="block w-full text-center py-2 rounded-md bg-blue-500 text-white hover:bg-blue-600">Submit</button>
        </div>
    </form>
    
    
    
</x-app-layout>