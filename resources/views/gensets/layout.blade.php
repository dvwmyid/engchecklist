<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Transport Log</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"
    />
    
</head>
<body>
    <div class="container">
        <div class="con">
            @yield('content')
        </div>
    </div>
    <script
        type="text/javascript"
        src="https://code.jquery.com/jquery-1.11.3.min.js"
    ></script>
    <script
    type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"
    ></script>
    <script>
        $(document).ready(function () {
          var date_input = $('input[name="date_run"]'); //our date input has the name "date"
          var container =
            $(".bootstrap-iso form").length > 0
              ? $(".bootstrap-iso form").parent()
              : "body";
          date_input.datepicker({
            format: "mm/dd/yyyy",
            container: container,
            todayHighlight: true,
            autoclose: true,
            clearBtn: true
          });
        });
    </script>
</body>
</html>