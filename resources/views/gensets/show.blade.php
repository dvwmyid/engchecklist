<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('List') }}
        </h2>
    </x-slot>
    
    
    <form action="{{ route('gensets.show', $genset->id) }}">
        <div class="row">
            <div class="">
                <div class="">
                    <Strong>Date:</Strong>
                    {{ $genset->created_at }}
                </div>
            </div>
            <div class="">
                <div class="">
                    <Strong>Speed:</Strong>
                    {{ $genset->oil_pressure }}
                </div>
            </div>
            <div class="">
                <div class="">
                    <Strong>Coolant Temperature</Strong>
                    {{ $genset->coolant_temp }}
                </div>
            </div>
            <div class="">
                <div class="">
                    <Strong>Battery Voltage</strong>
                    {{ $genset->battery_volt }}
                </div>
            </div>
            <div class="">
                <div class="">
                    <Strong>Run Time</Strong>
                    {{ $genset->run_time }}
                </div>
            </div>            
        </div>
    </form>
    
    
    
</x-app-layout>