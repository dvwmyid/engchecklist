<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $pageTitle }}
        </h2>
    </x-slot>
    
    @if ($message = Session::get('success'))
    <div class="alert alert-sucess">
        <p>{{ $message }}</p>
    </div>
    @endif

    <div class="flex justify-end">
        <a href="{{ route('transports.create') }}" class="text-sm px-3 py-2 text-white bg-blue-500 rounded">Create new Transport</a>
    </div>
    
    <table class="table-auto w-full mt-2">
        <tr>
            <th class="px-1 py-1 border-b border-gray-300 text-xs">No</th>
            <th class="px-1 py-1 border-b border-gray-300 text-xs">Date Run</th>
            <th class="px-1 py-1 border-b border-gray-300 text-xs">Driver</th>
            <th class="px-1 py-1 border-b border-gray-300 text-xs">Vehicle</th>
            <th class="px-1 py-1 border-b border-gray-300 text-xs">Purpose</th>
            <th class="px-1 py-1 border-b border-gray-300 text-xs">(KM)Before</th>
            <th class="px-1 py-1 border-b border-gray-300 text-xs">(KM)After</th>
            <th class="px-1 py-1 border-b border-gray-300 text-xs">Total Run</th>
            <th class="px-1 py-1 border-b border-gray-300 text-xs">(Time) From</th>
            <th class="px-1 py-1 border-b border-gray-300 text-xs">(Time) To</th>
            <th class="px-1 py-1 border-b border-gray-300 text-xs">Remarks</th>
            <th class="px-1 py-1 border-b border-gray-300 text-xs">Action</th>
        </tr>
        
        @foreach ($transports as $transport )
        
        <tr>
            <td class="px-1 py-1 border-b border-gray-300 text-xs">{{ $loop->index + 1 }}</td>
            <td class="px-1 py-1 border-b border-gray-300 text-xs">{{ $transport->date_run->format('d/m/y') }}</td>
            <td class="px-1 py-1 border-b border-gray-300 text-xs">{{ $transport->user->name }}</td>
            <td class="px-1 py-1 border-b border-gray-300 text-xs">{{ $transport->vehicle->name }}</td>
            <td class="px-1 py-1 border-b border-gray-300 text-xs">{{ $transport->purpose->description }}</td>
            <td class="px-1 py-1 border-b border-gray-300 text-xs">{{ number_format($transport->km_before, 2) }}</td>
            <td class="px-1 py-1 border-b border-gray-300 text-xs">{{ number_format($transport->km_after, 2) }}</td>
            <td class="px-1 py-1 border-b border-gray-300 text-xs">{{ number_format($transport->km_after - $transport->km_before, 2) }}Km</td>
            <td class="px-1 py-1 border-b border-gray-300 text-xs">{{ $transport->from_time->format('H:m') }}</td>
            <td class="px-1 py-1 border-b border-gray-300 text-xs">{{ $transport->to_time->format('H:m') }}</td>
            <td class="px-1 py-1 border-b border-gray-300 text-xs">{{ $transport->remarks }}</td>
            <td class="px-1 py-1 border-b border-gray-300 text-xs">
                <form action="{{ route('transports.destroy', $transport->id) }}" method="post" class="flex">
                    <a href="{{ route('transports.edit', $transport->id) }}" class="">
                    <i><svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="#60A5FA">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                      </svg></i></a>
                    
                    @csrf
                    @method('DELETE')
                    
                    <button type="submit" class=""><i><svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="#DB2777">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                      </svg></i></button>
                    
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    
    {!! $transports->links() !!}
    
</x-app-layout>