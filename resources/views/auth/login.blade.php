<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <script src="{{ mix('js/app.js')}}" defer></script>
</head>
<body>
    <form action="{{ route('login') }}" method="POST">
        <div class="w-full h-screen flex flex-col-reveresed">
            
            <img src="https://theanvayabali.com/contents/PSWTmcbOez4.jpg" alt="background" class="object-cover object-center h-screen w-7/12 shadow-lg">
            
            <div class="bg-white flex flex-col justify-center items-center w-5/12 shadow-lg">
                <h1 class="text-3xl font-bold text-blue-500 mb-2">LOGIN</h1>
                @csrf
                
                <div class="w-1/2 text-center">
                    <input type="text" name="username" placeholder="username" autocomplete="off" class="shadow-md border w-full h-10 px-3 py-2 text-black-500 focus:outline-none focus:border-blue-300 mb-3 rounded">
                    <input type="password" name="password" placeholder="password" autocomplete="off" class="shadow-md border w-full h-10 px-3 py-2 text-black-500 focus:outline-none focus:border-blue-300 mb-3 rounded">
                    <button class="bg-red-300 hover:bg-red-600 text-white px-3 py-1 rounded text-lg focus:outline-none shadow">Sign In</button>
                </div>
            </div>
        </div>
    </form>
</body>
</html>