<?php

namespace App\Http\Controllers;

use App\Models\Genset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class GensetController extends Controller
{
    public function index()
    {
        $gensets = Genset::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->paginate();
        $pageTitle = "Record list";
        return view('gensets.index', compact('gensets', 'pageTitle'));
    }

    public function create()
    {
        return view('gensets.create');
    }

    public function show(Genset $genset)
    {
        return view('gensets.show', compact('genset'));
    }

    public function store(Request $request)
    {
        $genset = new Genset;
        $genset->id = $request->get('id');
        $genset->user_id = Auth::user()->id;
        $genset->speed = $request->get('speed');
        $genset->oil_pressure = $request->get('oil_pressure');
        $genset->coolant_temp = $request->get('coolant_temp');
        $genset->battery_volt = $request->get('battery_volt');
        $genset->run_time = $request->get('run_time');

        $genset->save();

        return redirect()->route('gensets.show', ['genset' => $genset] );
    }

    public function edit(Genset $genset)
    {
        return view('gensets.edit', compact('genset'));
    }

    public function update(Request $request, Genset $genset)
    {
        $genset->speed = $request->get('speed');
        $genset->oil_pressure = $request->get('oil_pressure');
        $genset->collant_temp = $request->get('collant_temp');
        $genset->battery_volt = $request->get('battery_volt');
        $genset->run_time = $request->get('run_time');
        $genset->save();

        return redirect()->route('gensets.index');
    }

    public function destroy(Genset $genset)
    {
        try {
            $genset->delete();
        } catch (\Throwable $th) {
            abort(500);
        }

        return redirect()->route('genset.index');
    }
}
